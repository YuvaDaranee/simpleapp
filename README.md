## Application Overview

The Flask application in this repository is a simple web application that allows users to submit data through a form. Upon submission, the data is displayed on the next page. The application also includes a basic CI/CD pipeline for automated testing and deployment.

# Flask Application deployed by CI/CD and Jenkins

This repository is along with a CI/CD pipeline configured using GitLab to build and deploy once the commit has done.
Also this repository with a Jenkinsfile which contains jenkins pipeline script. Using this gitlab automatically trigger the jenkin build to deploy the app in local environment.

## CICD Pipeline in Gitlab

### Prerequisites
- Push your project to the gitlab repository.
- Create .gitlab-ci.yml file at the root of the repository to define CI/CD pipeline.

### Pipeline Stucture

Build:
Installs project dependencies using pip install -r requirements.txt.
Prepares the environment for testing and deployment.
Test:
Command used to test the code.
Deploy:
Deploys the application to a staging environment for further testing or review.

-Add image with proper python version as image: python:3.12
-Add stages to execute the jobs as per your required sequence.

### Configuration
- Gitlab repository is configured in configure section.
- The .gitlab-ci.yml file is configured to trigger pipeline runs automatically on every changes made to files in the repository.

## Jenkins Build

Installation: Install Jenkins on your server or local machine.
Creating new project: Create new pipeline project by doing below configurations.
### Configuration:
In configure section, select 'Pipeline from SCM', give correct gitlab repository url and crendentials and then proper jenkinsfile name/path.

Checkout: Checkout the source code from the GitLab repository.
Build Steps:
Install project dependencies using pip install -r requirements.txt.
Test:
Add some test commands
Deploy:
Run the Flask application using python3 app.py.

## Integrating jenkin with gitlab
### Access Token
1. Install Gitlab plugin in Jenkins.
2. Generate access token from gitlab and paste it in Gitlab section in the system configurations.
3. In jenkin, Get webhook url and secret token then paste it in webhook integration section in gitlab.
4. Test the connection to get success message.
5. Check the integration by committing changes to gitlab repository, it must reflect and trigger the jenkins build. 

### output
You will given localhost url to access the feedback form. You can fill the form and it will display your details in the next page.


By setting up both GitLab CI/CD and Jenkins for this project, we have established a robust automation pipeline for building and 
